﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MServices.V2.Protobuf
{
    public class ProtobufServiceClient : IServiceClient
    {
        public async Task<object> InvokeAsync(string url, MethodInfo method, object[] parameters)
        {
            TypemodelHelper.PrepareMethod(method);

            var request = WebRequest.CreateHttp(url);
            request.SendChunked = true;
            request.Method = "POST";
            request.ContentType = "application/octet-stream";

            var stream = await request.GetRequestStreamAsync();

            for (int i = 0; i < parameters.Length; i++)
            {
                ProtoBuf.Serializer.NonGeneric.SerializeWithLengthPrefix(stream, parameters[i], ProtoBuf.PrefixStyle.Base128, i + 1);
            }

            var response = await request.GetResponseAsync();
            var responseStream = response.GetResponseStream();

            var res = ProtoBuf.Serializer.Deserialize(method.ReturnType, responseStream);

            stream.Dispose();
            responseStream.Dispose();
            response.Dispose();

            return res;
        }
    }
}
