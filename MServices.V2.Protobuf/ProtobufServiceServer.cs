﻿using MServices.V2.Service;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MServices.V2.Protobuf
{
    public class ProtobufServiceServer : IServiceServer
    {
        public async Task InvokeAsync(ServiceEngine engine, ServiceMethod method, HttpListenerContext context)
        {
            var stream = context.Request.InputStream;
            var parameters = new object[method.ParameterTypes.Length];
            var idx = 0;

            if (parameters.Length > 0)
            {
                while (Serializer.NonGeneric.TryDeserializeWithLengthPrefix(stream, PrefixStyle.Base128, x => method.ParameterTypes[x - 1], out object value))
                {
                    parameters[idx++] = value;
                }
            }

            var invokeResult = await Task.Run(() =>
            {
                var serviceInstance = method.Serviceholder.CreateInstance(engine);
                return method.Method.Invoke(serviceInstance, parameters);
            });

            context.Response.SendChunked = true;
            context.Response.StatusCode = 200;
            context.Response.ContentType = "application/octet-stream";

            Serializer.NonGeneric.Serialize(context.Response.OutputStream, invokeResult);
        }
    }
}
