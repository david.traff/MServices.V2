﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MServices.V2.Protobuf
{
    public class ProtobufFlavor : IMServiceFlavor
    {
        public ProtobufFlavor()
        {
            Client = new ProtobufServiceClient();
            Server = new ProtobufServiceServer();
        }

        public IServiceClient Client { get; }

        public IServiceServer Server { get; }
    }
}
