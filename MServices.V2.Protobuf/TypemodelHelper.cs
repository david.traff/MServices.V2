﻿using ProtoBuf.Meta;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace MServices.V2.Protobuf
{
    public static class TypemodelHelper
    {
        private static readonly Dictionary<Type, bool> _PreparedTypes = new Dictionary<Type, bool>();
        private static readonly Dictionary<MethodInfo, bool> _PreparedMethods = new Dictionary<MethodInfo, bool>();
        private static readonly object _Lock = new object();

        public static void PrepareMethod(MethodInfo method)
        {
            lock (_Lock)
            {
                if (_PreparedMethods.ContainsKey(method))
                    return;

                Prepare(method.ReturnType);

                foreach (var parameter in method.GetParameters())
                {
                    Prepare(parameter.ParameterType);
                }

                //https://stackoverflow.com/questions/13735248/why-is-protobuf-so-slow-on-the-1st-call-but-very-fast-inside-loops
                //call CompileInPlace() "a few times" ¯\_(ツ)_/¯
                for (int i = 0; i < 5; i++)
                    RuntimeTypeModel.Default.CompileInPlace();
            }
        }

        public static void Prepare(Type type)
        {
            if (!_PreparedTypes.ContainsKey(type))
                PrepareType(type);
        }

        private static void PrepareType(params Type[] types)
        {
            foreach (var type in types)
            {
                if (type != null && !RuntimeTypeModel.Default.IsDefined(type))
                {
                    if (type.Namespace.StartsWith("System"))
                        return;

                    var props = type.GetProperties();
                    Array.Sort(props, (x, y) => string.Compare(
                        x.Name, y.Name, StringComparison.Ordinal));
                    var meta = RuntimeTypeModel.Default.Add(type, false);
                    int fieldNum = 1;
                    for (int i = 0; i < props.Length; i++)
                        if (props[i].CanWrite)
                        {
                            meta.Add(fieldNum++, props[i].Name);

                            if (!RuntimeTypeModel.Default.IsDefined(props[i].PropertyType))
                                if (props[i].PropertyType.HasElementType)
                                    PrepareType(props[i].PropertyType.GetElementType()); //T[]
                                else if (props[i].PropertyType.IsGenericType)
                                    PrepareType(props[i].PropertyType.GetGenericArguments()); //List<T>
                                else
                                    PrepareType(props[i].PropertyType);
                        }
                }
            }
        }
    }

}
