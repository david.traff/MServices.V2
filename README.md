# MServices.V2

A library that lets you treat your microservices as functions using dynamic proxies and [protobuf-net](https://github.com/mgravell/protobuf-net) for efficient serialization/deserialization of networked data.


## How do I use it

### Server-side example:
Implementing MServices is trivial. You just need to expose interfaces containing all the api-methods you want to expose and implement them in a normal class.
You then specify that you want to host these interfaces and your server-side is done!

```csharp
public class Foo
{
    public string Bar { get; set; }
}

public interface ITestService
{
    int Test(string a);
  
    Foo OtherTest(); 
}

public class TestService : ITestService
{

    public int Test(string a)
    {
        return a.Length;
    }
  
    public Foo OtherTest()
    {
        return new Foo()
        {
            Bar = "Hello!"
        };
    }
}
```

We then specify to host the service like this:

```csharp
class Program
{
    static void Main(string[] args)
    {
        var svcEngine = new ServiceEngine()
          .UsePort(1338)
          .HostService<ITestService, TestService>("/test/{method}")
          .Build();

        Console.ReadLine();
    }
}
```

### Client-side example
When you then want to consume this ``ITestService`` you either re-implement the interface itself and all it's custom argument/return-types or you can reference the server-side implementations. In this example we just redefine ``ITestService`` and ``Foo``.

```csharp
public class Foo
{
    public string Bar { get; set; }
}

public interface ITestService
{
    int Test(string a);
  
    Foo OtherTest(); 
}
```

We then consume the service like this:

```csharp
class Program
{
    static void Main(string[] args)
    {
        var svcEngine = new ServiceEngine()
          .ConsumeService<ITestService>("http://localhost:1338/test/{method}")
          .Build();
        
        var test = svcEngine.Service<ITestService>().Test("asd");// 3
        var otherTest = svcEngine.Service<ITestService>().OtherTest().Bar;// "Hello!"
        
        Console.ReadLine();
    }
}
```

It's really that simple.


## Current work

Changes from V1 is the use of protobuf and a custom implementation of a method to create interface proxies. I will continue to add some functionality for this and make the code prettier.

The main thing that needs to be implemented are middlewares which could allow for authentication and so on.
