﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace MServices.V2.JSON
{
    public class JSONServiceClient : IServiceClient
    {
        public Task<object> InvokeAsync(string url, MethodInfo method, object[] parameters)
        {
            var methodParameters = method.GetParameters();

            //The reason to use parameters.Length instead of methodParameters.Length is if
            //some non required arguments are left out, like string argument = ""
            var qs = new StringBuilder("?");
            for(int i = 0; i < parameters.Length; i++)
            {
                var parameterName = methodParameters[i].Name.ToLowerInvariant();

                var query = HttpUtility.UrlEncode($"{parameterName}={parameters[i]}");

                qs.Append($"{query},");
            }

            var querystring = qs.ToString().Trim(',');
            return null;
        }
    }
}
