﻿using MServices.V2;
using MServices.V2.Protobuf;
using MServices.V2.Service;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace MServices.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            var svcEngine = new ServiceEngine();

            svcEngine
                .UseFlavor<ProtobufFlavor>()
                .HostService<ITest, TestImplementation>("/test/{method}")
                .ConsumeService<ITest>("http://localhost:1338/test/{method}")
                .UsePort(1338)
                .SetAllowLocalConsume(false)
                .Build();

            for(; ; )
            {
                var count = 100;
                var sw = Stopwatch.StartNew();
                for(int i = 0; i < count; i++)
                {
                    var res = svcEngine.Service<ITest>().TestMethod(3);
                }
                sw.Stop();
                Console.WriteLine(sw.ElapsedMilliseconds / (double)count);
                Console.ReadLine();
            }
        }
    }

    public class TestImplementation : ITest
    {
        public Foo TestMethod(int k)
        {
            return new Foo()
            {
                Bar = "poop particle"
            };
        }
    }

    public interface ITest
    {
        Foo TestMethod(int k);
    }

    public class Foo
    {
        public string Bar { get; set; }
    }
}
