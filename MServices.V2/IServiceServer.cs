﻿using MServices.V2.Service;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MServices.V2
{
    public interface IServiceServer
    {
        Task InvokeAsync(ServiceEngine engine, ServiceMethod method, HttpListenerContext context);
    }
}
