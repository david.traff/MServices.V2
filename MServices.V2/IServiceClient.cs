﻿using System;
using System.Reflection;
using System.Threading.Tasks;

namespace MServices.V2
{
    public interface IServiceClient
    {
        Task<object> InvokeAsync(string url, MethodInfo method, object[] parameters);
    }
}
