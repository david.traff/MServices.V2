﻿using MServices.V2.Service;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace MServices.V2
{
    public class ServiceEngine
    {
        public ServiceEngine()
        {
            _AllowBuild = true;
        }

        private readonly Dictionary<Type, ServiceHolder> _HostedServices = new Dictionary<Type, ServiceHolder>();
        public IReadOnlyDictionary<Type, ServiceHolder> HostedServices => _HostedServices;
        private ConcurrentDictionary<string, ServiceMethod> _MethodCache = new ConcurrentDictionary<string, ServiceMethod>();

        private readonly Dictionary<Type, ServiceHolder> _ConsumedServices = new Dictionary<Type, ServiceHolder>();
        public IReadOnlyDictionary<Type, ServiceHolder> ConsumedServices => _ConsumedServices;


        private IServiceClient _ServiceClient;
        public IServiceClient ServiceClient => _ServiceClient;

        private IServiceServer _ServiceServer;
        public IServiceServer ServiceServer => _ServiceServer;

        private bool _AllowBuild;
        private bool _AllowLocalConsume = true;
        private int _Port = 1337;

        private ServiceServer _Server;

        public bool AllowLocalConsume => _AllowLocalConsume;
        public int Port => _Port;

        /*internal*/public object HandleProxyMethodCall(object[] arguments, object instance, string methodName)
        {
            var instanceType = instance.GetType();
            var method = instanceType.GetMethod(methodName);
            var interfaceType = instanceType.GetInterfaces()[0];
            var service = _ConsumedServices[interfaceType];
            var url = service.URL.Replace(ServiceMethod.MethodPlaceholder, method.Name.ToLowerInvariant());
            
            var responseTask = _ServiceClient.InvokeAsync(url, method, arguments);

            responseTask.Wait();
            responseTask.Dispose();

            return responseTask.Result;
        }

        public T Service<T>()
        {
            if (_AllowBuild)
                throw new Exception("You need to build this engine before you can use it.");

            //if we allow local consume and it is hosted by this engine.
#pragma warning disable IDE0018 // Inline variable declaration
            ServiceHolder service = null;
#pragma warning restore IDE0018 // Inline variable declaration
            if (_AllowLocalConsume && _HostedServices.TryGetValue(typeof(T), out service))
            {
                return (T)service.CreateInstance(this);
            }//else if we are consuming this service
            else if (_ConsumedServices.TryGetValue(typeof(T), out service))
            {
                return (T)service.CreateInstance(this);
            }

            throw new Exception($"'{typeof(T).FullName}' is not being consumed or hosted (check that SetAllowLocalConsume is set to true if you want to consume a locally hosted service)");
        }

        #region build
        private void EnsureAllowBuild()
        {
            if (!_AllowBuild)
                throw new InvalidOperationException("dont do this");
        }

        public ServiceEngine SetAllowLocalConsume(bool allow)
        {
            EnsureAllowBuild();
            _AllowLocalConsume = allow;

            return this;
        }

        public ServiceEngine UsePort(int port)
        {
            EnsureAllowBuild();
            _Port = port;

            return this;
        }

        /// <summary>
        /// Specifies to host the given service.
        /// </summary>
        /// <typeparam name="T">Type of service which will be hosted.</typeparam>
        /// <typeparam name="U">Implementation of the service which will be hosted.</typeparam>
        /// <param name="url">Specifies the URL at which to listen on.</param>
        /// <returns></returns>
        public ServiceEngine HostService<T, U>(string url) where U : T
        {
            EnsureAllowBuild();
            var typeofT = typeof(T);

            if (!typeofT.IsInterface)
                throw new ArgumentException($"{typeofT.FullName} is not an interface");

            if (_HostedServices.ContainsKey(typeofT))
                throw new ArgumentException($"{typeofT.FullName} is already being hosted");

            if (string.IsNullOrEmpty(url) || url.ToLowerInvariant().IndexOf(ServiceMethod.MethodPlaceholder) == -1)
                throw new ArgumentException("Invalid hosted URL");

            _HostedServices.Add(typeofT, new HostedService(typeofT, typeof(U), url.ToLowerInvariant()));

            return this;
        }

        public ServiceEngine ConsumeService<T>(string url)
        {
            EnsureAllowBuild();
            var typeofT = typeof(T);

            if (!typeofT.IsInterface)
                throw new ArgumentException($"{typeofT.FullName} is not an interface");

            if (_ConsumedServices.ContainsKey(typeofT))
                throw new ArgumentException($"{typeofT.FullName} is already being consumed");

            if (string.IsNullOrEmpty(url) || url.ToLowerInvariant().IndexOf(ServiceMethod.MethodPlaceholder) == -1)
                throw new ArgumentException("Invalid hosted URL");

            _ConsumedServices.Add(typeofT, ProxyHandler.CreateProxy<T>(url.ToLowerInvariant()));

            return this;
        }

        public ServiceEngine UseFlavor<T>() where T : IMServiceFlavor
        {
            EnsureAllowBuild();

            var instance = Activator.CreateInstance<T>();

            _ServiceClient = instance.Client;
            _ServiceServer = instance.Server;

            return this;
        }

        public ServiceEngine Build()
        {
            EnsureAllowBuild();

            if (_ServiceClient == null)
                throw new InvalidOperationException("You need to specify a ServiceClient");

            if (_ServiceServer == null)
                throw new InvalidOperationException("You need to specify a ServiceServer");

            _AllowBuild = false;

            foreach(var service in _HostedServices.Values)
            {
                foreach(var method in service.ServiceMethods)
                {
                    if (_MethodCache.ContainsKey(method.URL))
                        throw new InvalidOperationException($"There are two conflicting services with the URL {method.URL}");

                    _MethodCache.TryAdd(method.URL, method);
                }
            }

            _Server = new ServiceServer(this, _Port);
            _Server.Start();

            return this;
        }
        #endregion

        public bool TryGetServiceMethod(string url, out ServiceMethod method) => _MethodCache.TryGetValue(url.ToLowerInvariant(), out method);
    }
}
