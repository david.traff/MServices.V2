﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MServices.V2
{
    public interface IMServiceFlavor
    {
        IServiceClient Client { get; }

        IServiceServer Server { get; }
    }
}
