﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MServices.V2.Service
{
    public sealed class HostedService : ServiceHolder
    {
        public HostedService(Type interfaceType, Type implementedType, string url) : base(interfaceType, url)
        {
            if (!implementedType.IsClass)
                throw new ArgumentException(nameof(implementedType));

            ImplementedType = implementedType;
        }
    }
}
