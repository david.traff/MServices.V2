﻿using MServices.V2.Service;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MServices.V2.Service
{
    public class ServiceServer
    {
        public ServiceServer(ServiceEngine engine, int port)
        {
            _Engine = engine;
            _Port = port;
        }

        private int _Port;
        private ServiceEngine _Engine;
        private HttpListener _Listener;
        private Thread _ListenerThread;

        public void Start()
        {
            _ListenerThread = new Thread(Listen);
            _ListenerThread.Start();
        }

        private void Listen()
        {
            _Listener = new HttpListener();
            _Listener.Prefixes.Add($"http://localhost:{_Port}/");
            _Listener.Start();

            while (_Listener.IsListening)
            {
                var ctx = _Listener.BeginGetContext(OnContext, _Listener);
                ctx.AsyncWaitHandle.WaitOne();
            }
        }

        private async void OnContext(IAsyncResult result)
        {
            var context = _Listener.EndGetContext(result);

            try
            {
                if (_Engine.TryGetServiceMethod(context.Request.RawUrl, out ServiceMethod method))
                {
                    await _Engine.ServiceServer.InvokeAsync(_Engine, method, context);
                }
                else
                {
                    context.Response.StatusCode = 404;
                }
            }
            catch (Exception e)
            {
                context.Response.StatusCode = 500;

                Console.WriteLine(e.ToString());
            }
            finally
            {
                context.Request.InputStream.Dispose();
                context.Response.OutputStream.Dispose();
                context.Response.Close();
            }
        }
    }
}
