﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace MServices.V2.Service
{
    public class ServiceMethod
    {
        public const string MethodPlaceholder = "{method}";

        public ServiceMethod(MethodInfo method, ServiceHolder holder)
        {
            Serviceholder = holder;
            Method = method;
            ParameterTypes = method.GetParameters().Select(x => x.ParameterType).ToArray();
        }

        public ServiceHolder Serviceholder { get; }

        public MethodInfo Method { get; }
        
        public Type[] ParameterTypes { get; }

        public string URL => Serviceholder.URL.Replace(MethodPlaceholder, Method.Name).ToLowerInvariant();
    }
}
