﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;

namespace MServices.V2.Service
{
    public static class ProxyHandler
    {
        private static AssemblyName _AssemblyName = new AssemblyName("mservices.generatedtypes");
        private static AssemblyBuilder _AssemblyBuilder = AssemblyBuilder.DefineDynamicAssembly(_AssemblyName, AssemblyBuilderAccess.Run);
        private static ModuleBuilder _ModuleBuilder = _AssemblyBuilder.DefineDynamicModule("generatedtypes");

        //TODO: make this cache created types.

        public static InterfaceProxy CreateProxy(Type type, string url)
        {
            if (!type.IsInterface)
                throw new ArgumentException(nameof(type));

            var proxy = new InterfaceProxy(type, url, _ModuleBuilder);

            return proxy;
        }
        
        public static InterfaceProxy CreateProxy<T>(string url) => CreateProxy(typeof(T), url);
    }
}
