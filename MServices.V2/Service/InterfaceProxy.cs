﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;

namespace MServices.V2.Service
{
    public class InterfaceProxy : ServiceHolder
    {
        internal InterfaceProxy(Type type, string url, ModuleBuilder moduleBuilder) : base(type, url)
        {
            BuildProxy(moduleBuilder);
        }

        public override object CreateInstance(ServiceEngine engine)
        {
            return Activator.CreateInstance(ImplementedType, engine);
        }

        #region il-gen
        private void BuildProxy(ModuleBuilder moduleBuilder)
        {
            var methodInfos = InterfaceType.GetMethods();
            var ns = typeof(ServiceEngine).Namespace;
            var typeBuilder = moduleBuilder.DefineType(ns + ".generated_" + InterfaceType.Name, TypeAttributes.Public);
            typeBuilder.AddInterfaceImplementation(InterfaceType);

            var ctorBuilder = typeBuilder.DefineConstructor(
                      MethodAttributes.Public,
                      CallingConventions.Standard,
                      new Type[] { typeof(ServiceEngine) });

            var field = typeBuilder.DefineField("_InterfaceProxy", typeof(ServiceEngine), FieldAttributes.Private);
            var ilGenerator = ctorBuilder.GetILGenerator();

            //object-reference
            ilGenerator.Emit(OpCodes.Ldarg_0);
            //ServiceEngine
            ilGenerator.Emit(OpCodes.Ldarg_1);
            //store argument (InterfaceProxy) in field
            ilGenerator.Emit(OpCodes.Stfld, field);

            ilGenerator.Emit(OpCodes.Ret);

            foreach (var methodInfo in methodInfos)
            {
                GenerateMethod(methodInfo, typeBuilder, field);
            }

            ImplementedType = typeBuilder.CreateTypeInfo();
        }

        private void GenerateMethod(MethodInfo method, TypeBuilder builder, FieldBuilder fieldbuilder)
        {
            var methodParams = method.GetParameters().Select(x => x.ParameterType).ToArray();
            var methodBuilder = builder.DefineMethod(
                method.Name,
                MethodAttributes.Public | MethodAttributes.Virtual | MethodAttributes.CheckAccessOnOverride,
                method.ReturnType,
                methodParams);


            var generator = methodBuilder.GetILGenerator();

            GenerateLocals(generator, method, methodParams);
            GenerateHead(generator, method, fieldbuilder);
            GenerateArguments(generator, method, methodParams, fieldbuilder);
            GenerateInvoke(generator, method, fieldbuilder);

            builder.DefineMethodOverride(methodBuilder, method);
        }

        private void GenerateLocals(ILGenerator generator, MethodInfo method, Type[] methodParams)
        {
            //om metoden har retur-typ så definierar vi upp en local som representerar retur-objektet.
            if (method.ReturnType != typeof(void))
                generator.DeclareLocal(method.ReturnType);


            //om metoden har parametrar så definierar vi upp en local som kommer att innehålla dem.
            if (methodParams.Length > 0)
                generator.DeclareLocal(typeof(object[]));
        }

        private void GenerateHead(ILGenerator generator, MethodInfo method, FieldBuilder fieldbuilder)
        {
            generator.Emit(OpCodes.Nop);

            //generator.Emit(OpCodes.Ldarg_0);
            //generator.Emit(OpCodes.Ldarg_0);
            //generator.Emit(OpCodes.Ldfld, fieldbuilder);

            //oklart.
            if (method.ReturnType != typeof(void))
                generator.Emit(OpCodes.Ldarg_0);
        }

        private void GenerateArguments(ILGenerator generator, MethodInfo method, Type[] methodParams, FieldBuilder fieldbuilder)
        {
            //om det inte finns nå argument så räcker ju ovan. Arrayen kommer då att vara null.
            if (methodParams.Length == 0)
            {
                //ev-stacken kommer att vara: str, str, null
                generator.Emit(OpCodes.Ldnull);
            }
            else
            {
                //skapa upp en int som motsvarar längden på object[]-arrayen.
                generator.Emit(OpCodes.Ldc_I4_S, methodParams.Length);

                //skapa upp en ny array av typen "object". Längden är pushad på ev-stacken ovan.
                generator.Emit(OpCodes.Newarr, typeof(object));

                //eftersom arrayen är en local så beror det på om man har deklarerat en retur-typ
                //i GenerateLocals.
                var hasReturn = method.ReturnType != typeof(void);

                //vilket idx som arrayen ska till i locals när den flyttas från ev-stacken.
                var set_op = hasReturn ? OpCodes.Stloc_1 : OpCodes.Stloc_0;

                //vilket idx som arrayen ligger på när den ska flyttas TILL ev-stacken.
                var get_op = hasReturn ? OpCodes.Ldloc_1 : OpCodes.Ldloc_0;

                //först vill vi ju flytta den ny-skapta arrayen till locals.
                generator.Emit(set_op);

                for(int i = 0; i < methodParams.Length; i++)
                {
                    //hämta arrayen från locals.
                    generator.Emit(get_op);

                    //oklart.
                    generator.Emit(OpCodes.Ldc_I4_S, i);

                    //ladda argument till ev-stacken.
                    //i + 1 offset eftersom 0 är pekaren till den klass vi skapar upp.
                    generator.Emit(OpCodes.Ldarg_S, i + 1);


                    //om det är en value-typ så boxar vi in argumentet till en referens.
                    if (methodParams[i].IsValueType)
                        generator.Emit(OpCodes.Box, methodParams[i]);

                    //replacear elementet i arrayen vid idx från "Ldc_I4_S, i"
                    generator.Emit(OpCodes.Stelem_Ref);
                }

                //hämta arrayen från locals igen så den ligger bland alla argument.

                //förbereder argument för proxyhandlern
                generator.Emit(OpCodes.Ldfld, fieldbuilder);

                generator.Emit(get_op);
            }

            //förbereder argument for namnet av metoden.
            generator.Emit(OpCodes.Ldarg_0);
            generator.Emit(OpCodes.Ldstr, method.Name);
        }

        private void GenerateInvoke(ILGenerator generator, MethodInfo method, FieldBuilder fieldbuilder)
        {
            //var methodToInvoke = typeof(ServiceEngine).GetMethod(nameof(ServiceEngine.HandleProxyMethodCall), BindingFlags.NonPublic | BindingFlags.Instance);
            var methodToInvoke = typeof(ServiceEngine).GetMethod(nameof(ServiceEngine.HandleProxyMethodCall));

            generator.Emit(OpCodes.Callvirt, methodToInvoke);

            if (method.ReturnType != typeof(void))
            {
                generator.Emit(OpCodes.Stloc_0);
                generator.Emit(OpCodes.Ldloc_0);
            }
            else
            {
                generator.Emit(OpCodes.Pop);
            }

            generator.Emit(OpCodes.Ret);
        }
        #endregion
    }
}
