﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MServices.V2.Service
{
    public abstract class ServiceHolder
    {
        public ServiceHolder(Type interfaceType, string url)
        {
            if (!interfaceType.IsInterface)
                throw new ArgumentException(nameof(interfaceType));

            InterfaceType = interfaceType;
            URL = url;

            var methods = interfaceType.GetMethods();
            ServiceMethods = new ServiceMethod[methods.Length];

            for(int i = 0; i < methods.Length; i++)
            {
                ServiceMethods[i] = new ServiceMethod(methods[i], this);
            }
        }

        public virtual object CreateInstance(ServiceEngine engine) => Activator.CreateInstance(ImplementedType);

        public Type InterfaceType { get; }

        public string URL { get; }

        public Type ImplementedType { get; protected set; }

        public ServiceMethod[] ServiceMethods { get; }
    }
}
